﻿import numpy as np
import re
import os
from sklearn import svm
import math
from collections import defaultdict


FIRST_N_WORDS = 1000

# Acuratetea = cate etichete prezise sunt egale cu ce e adevarat?
def acuratete(predicted_labels, what_is_true):
    predicted_labels = np.array(predicted_labels)
    what_is_true = np.array(what_is_true)

    boolean_index = (predicted_labels == what_is_true)  # vector cu true/false. Pastreaza doar True

    return len(what_is_true[boolean_index]) * 100 / len(what_is_true)

    # metoda 2
    # accuracy = (etichete_prezise == what_is_true).astype('int').mean() #cate sunt corecte fata de cate sunt in total
    # return accuracy * 100


def files_in_folder(path):
    files = []
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            files.append(os.path.join(path, file))
    return sorted(files)


def files_without_extension(path):
    file_name = os.path.basename(path)
    file_name_without_extension = file_name.replace('.txt', '')
    return file_name_without_extension


def read_texts_from_directory(path):
    text_data = []
    text_id = []
    for file in files_in_folder(path):
        text_id.append(files_without_extension(file))
        with open(file, 'r', encoding='utf-8') as f:
            text = f.read()
        # incercati cu si fara punctuatie sau cu lowercase

        # text_stripped = re.sub("[-.,;:!?\"\'\/()_*=`]", "", text)
        # text_data.append(text_stripped.split())

        # text = text.lower()
        text_data.append(text.split())

    return (text_id, text_data)


# incarcam datele
dir_path = 'trainData//'
labels = np.loadtxt(dir_path + 'labels_train.txt')
train_examples_path = os.path.join(dir_path, 'trainExamples')

text_id, text_data = read_texts_from_directory(train_examples_path)

# print("primele 10 cuvinte din primul text\n", text_data[0][:10])

#imparte datele in antrenare, validare, testare
trainExamples = 2000
validationExamples = 500
testExamples = len(text_data) - (trainExamples + validationExamples)

dataTrain = text_data[:trainExamples]
labelsTrain = labels[:trainExamples]

dataValidation = text_data[trainExamples:trainExamples+validationExamples]
labelsValidation = labels[trainExamples:trainExamples+validationExamples]

dataTest = text_data[trainExamples+validationExamples:]
labelsTest = labels[trainExamples+validationExamples:]

# histograme
indici_train = np.arange(0, trainExamples)
indici_valid = np.arange(trainExamples, trainExamples + validationExamples)
indici_test = np.arange(trainExamples + validationExamples, len(text_data))

print ("Histograma cu clasele din train: ", np.histogram(labels[indici_train])[0])
print ("Histograma cu clasele din validation: ", np.histogram(labels[indici_valid])[0])
print ("Histograma cu clasele din test: ", np.histogram(labels[indici_test])[0])


# construim un dictionar fol toate cuvintele unice din ex de antrenare cu frecventa cuvintelor
word_dictionary = defaultdict(int)
for text in text_data:
    for word in text:
        word_dictionary[word] += 1

# transformam dictionarul in lista de tupluri ['cuvant1', frecventa1, 'cuvant2': frecventa2]
words_frequencies = list(word_dictionary.items())

# sortam descrescator lista de tupluri dupa frecventa
words_frequencies = sorted(words_frequencies, key = lambda kv: kv[1], reverse = True)

# extragem primele 1000 cele mai frecvente cuvinte din toate textele
small_word_dictionary = words_frequencies[0: FIRST_N_WORDS]

print("Primele 10 cele mai frecv cuvinte", small_word_dictionary[0:10])


list_of_selected_words = []
for cuvant, frecventa in small_word_dictionary:
    list_of_selected_words.append(cuvant)

# print(list_of_selected_words)


# Repr BOW pt un text impartit in cuvinte in functie de list_of_selected_words


def get_bow_representation_for_text(text, list_of_selected_words):
    bow_representation = dict()
    for word in list_of_selected_words:
        bow_representation[word] = 0
    for word in text:
        if word in list_of_selected_words:
            bow_representation[word] += 1
    return bow_representation


# print(get_bow_representation_for_text(text_data[0], list_of_selected_words))

# Repr BOW normalizata pt o matrice de exemple in functie de list_of_selected_words


def get_bow_representation_for_matrix(matrix, list_of_selected_words, normalizare):
    bow_representation = np.zeros((len(matrix), len(list_of_selected_words)))
    for index_text, text in enumerate(matrix):
        bow_dict = get_bow_representation_for_text(text, list_of_selected_words)
        # bow_dict e dictionar, trebuie convertit in lista, apoi in np.array.  bow_dict.values() e un obiect de tipul dict_values
        bow = np.array(list(bow_dict.values()))
        if normalizare == 'l2':
            bow = bow / np.sqrt(np.sum(bow ** 2))
        elif normalizare == 'l1':
            bow = bow / np.sum(bow)
        # else:
        #     print('Tip normalizare incorect!')
        #     return
        bow_representation[index_text] = bow
    return bow_representation


data_bow_representation = get_bow_representation_for_matrix(text_data, list_of_selected_words)
print("Text Data bow representation shape: ", data_bow_representation.shape)


# Antrenare clasificator
#  Pentru valori mari ale lui C -> hiperplan cu margine mai mică dar cu acuratețe cât mai mare a clasificării pe exemplele din
# mulțimea de antrenare. Pentru valori mai mici ale lui C va fi ales un hiperplan cu o margine mai mare, chiar dacă acesta duce
# la clasificarea greșită a unor exemple din mulțimea de antrenare.

# antrenare clasificator cu train Data, preziere pt validation Data
for C in [0.001, 0.01, 0.1, 1, 10, 100]:
    clasificator = svm.SVC(C, 'linear')
    # antrenare model
    clasificator.fit(data_bow_representation[indici_train, :], labels[indici_train])
    # predictie
    predictii = clasificator.predict(data_bow_representation[indici_valid, :])
    print("Acuratete pe validare cu C=", C, ":", acuratete(predictii, labels[indici_valid]))
print('###################################################')

#  concatenam indicii de train si validare
indici_train_valid = np.concatenate([indici_train, indici_valid])

# antrenare clasificator cu train data, validation data, prezicere pt test data
for C in [0.001, 0.01, 0.1, 1, 10, 100]:
    clasificator = svm.SVC(C, 'linear')
    # antrenare model
    clasificator.fit(data_bow_representation[indici_train_valid, :], labels[indici_train_valid])
    # predictie
    predictii = clasificator.predict(data_bow_representation[indici_test, :])
    print("Acuratete pe validare cu C=", C, ":", acuratete(predictii, labels[indici_test]))
print('###################################################')


